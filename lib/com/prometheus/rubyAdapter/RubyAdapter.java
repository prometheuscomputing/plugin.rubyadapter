
package com.prometheus.rubyAdapter;

import java.util.*; //for debug
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;

import java.net.URL;
import java.net.URI;
import java.net.URLClassLoader;

import java.lang.reflect.Method;

import java.util.logging.Logger;
import java.util.Properties;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.nomagic.actions.AMConfigurator;
import com.nomagic.actions.ActionsManager;
import com.nomagic.magicdraw.actions.ActionsConfiguratorsManager;
import com.nomagic.magicdraw.actions.BrowserContextAMConfigurator;
import com.nomagic.magicdraw.actions.MDActionsCategory;
import com.nomagic.magicdraw.plugins.Plugin;
import com.nomagic.magicdraw.ui.browser.Tree;


/*

The Ruby adapter is configured by environmental variables or a properties file.

If you want use environmental variables, be aware that on MacOS variables present in the shell are not available to apps.
To pass environmental variables to apps on MacOS, you can use:
  * https://github.com/hschmidt/EnvPane/releases/tag/releases%2F0.6  (tested on High Sierra, 10.13.1)
  * https://github.com/ersiner/osx-env-sync (this did not work for Art on High Sierra)
    If you want to change variables, you need to:
      * launchctl unload ~/Library/LaunchAgents/osx-env-sync.plist
      * launchctl load ~/Library/LaunchAgents/osx-env-sync.plist

If you want use a properties file:
  * The optional properties file must be placed at the path specified by
    properties_filepath():  "~/Prometheus/RubyAdapter.properties".
  * The properties file format is: https://en.wikipedia.org/wiki/.properties.
  * Properties names are identical to environmental variable names

If both methods are used (not a good idea), the environmental variables take precidience.

There are several configuration strategies, defined below.
* Power users should immediately jump to Method 3.
* If you have rvm installed, Method 2 is easiest.
* Method 1 will become the default for third paries using our tools.

=============================================================================
Method 1  (May eventually become the preferred method)
=============================================================================
JRuby Complete Jar. Uses Ruby core and standard libraries provided by JRuby Complete, and additional plugin gems (currently provided by rvm gemset).
   QUESTION: Why would you use a JRuby jar if you have to have JRuby installed in RVM anyway in order to create and populate a gemset???
   ANSWERS:
       * It's a first step in running MagicDraw plugins from ONLY a jar (without rvm).
         That's desirable because it's easier to give a new user a jar than to install rvm.  Consider Windows users for example.
         Of course that requires adding extra plugin gems to the jar.
         We have produced experimental versions of JRuby Complete with extra gems: it's not documented yet.
       * The RubyAdapter isn't just for MagicDraw.  In fact it was originally developed for the UML-driven translator gui (without gemsets).
         Yes, we haven't supported that in a while - and that doesn't mean it should be thrown away.
Optional environmental variables:
   MD_RUNTIME_TYPE - must be unspecified, or "jar"
   MD_RUBY_ROOT - defaults to "~/.rvm/".  If specified, use absolute path (not '~')
   MD_RVM_JRUBY_VERSION - defaults to RubyAdapter::DEFAULT_RVM_JRUBY_VERSION  # Used to find code in gemsets managed by rvm
   MD_JAR_JRUBY_VERSION - defaults to RubyAdapter::DEFAULT_RVM_JRUBY_VERSION  # Used to find scripting engine
   MD_GEMSET_NAME - defaults to MagicDraw
   MD_DEV_DIR - defaults to empty string
Values computed for you (if you want to override, use "Full control, low level" method instead):
   MD_RubyLibPath - fragment used to assemble other paths: rubyRoot + "/rubies/jruby-" + rvm_jruby_version + "/lib/"
   MD_JAR_PATH - jarchive + "jruby-complete-" + jar_jruby_version + ".jar"
   MD_EMBED_CLASSPATH - empty string
   MD_GEM_GLOBS - computed from "Optional evironmental variables" (same as MD_RUNTIME_TYPE="rvm")
   MD_EXTRA_LOADPATHS - empty string

=============================================================================
Method 2  (Currently most typical)
=============================================================================
RVM Provided JRuby core and standard libraries, and plugin gems provided by rvm gemset.
Required environmental variable:
   MD_RUNTIME_TYPE - must set to "rvm"
Optional evironmental variables:
   MD_RUBY_ROOT - defaults to "~/.rvm/".  If specified, use absolute path (not '~')
   MD_RVM_JRUBY_VERSION - defaults to RubyAdapter::DEFAULT_RVM_JRUBY_VERSION  # Used to find scripting engine & code in gemsets managed by rvm
   MD_JAR_JRUBY_VERSION - defaults to RubyAdapter::DEFAULT_RVM_JRUBY_VERSION  # Used to find scripting engine (there has been at least one case where this was different from MD_RVM_JRUBY_VERSION)
   MD_GEMSET_NAME - defaults to MagicDraw
   MD_DEV_DIR - defaults to empty string
Values computed for you (if you want to override, use "Full control, low level" method instead):
   MD_RubyLibPath - fragment used to assemble other paths: rubyRoot + "/rubies/jruby-" + rvm_jruby_version + "/lib/"
   MD_JAR_PATH - computed path of jar installed by rvm: rubyLibPath + "jruby.jar"
   MD_EMBED_CLASSPATH - computed: rubyLibPath + "ruby/stdlib"
   MD_GEM_GLOBS - computed from "Optional evironmental variables" (same as MD_RUNTIME_TYPE="jar")
   MD_EXTRA_LOADPATHS - computed: "#{rubyLibPath}ruby/shared"

=============================================================================
Method 3 (Full control, low level)
=============================================================================
Required environmental variables:
   MD_JAR_PATH - Absolute path to one extra jar file. Not used if empty string.
   MD_EMBED_CLASSPATH - Value for system property "org.jruby.embed.class.path" used by jruby. Not used if empty string.
   MD_GEM_GLOBS - Used to find load paths for gems.  A File::PATH_SEPARATOR delimited list of glob patterns. Each glob is expanded to obtain gem lib directories which are appended to the Ruby $LOAD_PATH
   MD_EXTRA_LOADPATHS - Appended to the Ruby $LOAD_PATH (after directories found by MD_GEM_GLOBS)
Optional environmental variables:
   MD_DEV_DIR - contains path to a directory that contains sym links to projects that override gems (go on the load path first). Defaults to empty string.

*/

/**
 * Plugin that calls Ruby code in scripts/main.rb via JRuby.
 */
public class RubyAdapter extends Plugin {

	
  private final static String DEFAULT_RVM_JRUBY_VERSION = "9.2.0.0"; 

  // This Map-like object is loaded from properties_filepath (if present).
  // It serves as an altarnative to environmental variables.
  private Properties properties;
    
  // MD_RUNTIME_TYPE.  Specifies how some defaults are computed. 
  // Either "jar" (jruby complete in a jar) or "rvm" (jruby libraries installed by rvm).  If unspecified, behaves like "jar"
  private String runtime_type;
  
  // MD_RUBY_ROOT.  Locates gemset (possibly also runtime). If specified, must be absolute. Don't use "~"
  private String rubyRoot;
  
  // MD_RVM_JRUBY_VERSION.  Locates gemset (possibly also runtime)
  private String rvm_jruby_version;
  
  // MD_JAR_JRUBY_VERSION.  Locates scripting engine
  private String jar_jruby_version;
  
  // MD_JAR_PATH.  Path to one extra jar.
  private String jar_path;
  
  // MD_RubyLibPath.  Used by rvm provided runtime
  private String rubyLibPath;
  
  // MD_EMBED_CLASSPATH.  Used by rvm provided runtime
  private String ruby_std_lib;
	
	// JARCHIVE.  Default location of jruby complete jar, when runtime_type is "jar"
	private String jarchive;


  // There are also the following variables, initialized in Ruby code:
  //    gemset_name can be set by MD_GEMSET_NAME
  //    gem_globs can be set by MD_GEM_GLOBS. Used to find gem lib directories that are appended to $LOAD_PATH. Example: "/Users/griesser/.rvm/gems/jruby-9.2.0.0@MagicDraw/gems/*/lib:/Users/griesser/.rvm/gems/jruby-9.2.0.0@global/gems/*/lib"
  //    extra_loadPaths can be set by MD_EXTRA_LOADPATHS.  Appended to $LOAD_PATH. Example is "/Users/griesser/.rvm/rubies/jruby-9.2.0.0/lib/ruby/shared" 
	

  // =======================================================================
  // Some configuration helper methods
  
  public String home_dir() {
    return System.getenv("HOME");
  }

  // If present, the properties file provides an alternative way to specify configuration.
  // It's useful because environmental variables for applications are poorly implmented on MacOS.
  // The properties file is specified by this format: https://en.wikipedia.org/wiki/.properties
  public String properties_filepath() { return home_dir() + "/Prometheus/RubyAdapter.properties"; }

  public void init_properties() {
    properties = new Properties();
    try {
      this.properties.load(new FileInputStream(properties_filepath()));
      System.out.println("\nSuccessfully parsed property file " + properties_filepath() );
    } catch (IOException  e) {
      // Do nothing - properties remains empty
    }
  }

  // Tries to obtain specified configuration parameter first from environmental variables,
  // then from properties file.  If not found in either place uses default value provied by deflt.
  public String config_var(String name, String deflt) {
    String env_val = System.getenv(name);
    return (null != env_val) ? env_val : properties.getProperty(name, deflt);
  }

  // This version provides a fallback in case deflt is null.
  public String config_var(String name, String deflt, String deflt2) {
    String env_val = config_var(name, deflt);
    return (null != env_val) ? env_val : deflt2;
  }

  // Show all environmental variables for debugging purposes
  public void show_env_vars() {
    System.out.println("\nEnvironmental variables: ");
    Map<String, String> env = System.getenv();
    for (String envName : env.keySet()) {
        System.out.format("                  %s=%s%n", envName, env.get(envName));
    }
  }

  // Try to get the jruby version (presumes it's available from rvm)
  public String find_jruby_version() {
    String found_jruby_version = null;
    try {
      Runtime rt      = Runtime.getRuntime();
      Process rvmlist = rt.exec(rubyRoot + "/bin/rvm list strings");
      rvmlist.waitFor();
      BufferedReader brdr = new BufferedReader(new InputStreamReader(rvmlist.getInputStream()));
      String rvmlistline = "";
      while ((rvmlistline = brdr.readLine()) != null) {
        if (rvmlistline.matches(".*jruby-9.*")) {
          String version = rvmlistline.substring(6,rvmlistline.length());
          if (found_jruby_version == null) {
            found_jruby_version = version;
          } else {
              Version this_version = new Version(version);
              Version that_version = new Version(found_jruby_version);
              if (this_version.compareTo(that_version) > 0) {
                found_jruby_version = version;
              }
          }
        }
      }
      brdr.close();
    } catch (Throwable e) {
      // do nothing
    }
    return found_jruby_version;
  }

  public static void addFileToClasspath(final String path) throws Throwable {
		System.out.println("Adding to CLASSPATH: '" + path + "'");
    final File file                = new File(path);
    boolean exists = file.exists();
    System.out.println("File to be added to CLASSPATH exists: " + exists );
    final URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    final Class<?> sysclass        = URLClassLoader.class;
    final Method method            = sysclass.getDeclaredMethod("addURL", new Class<?>[] {URL.class});
    method.setAccessible(true);
    method.invoke(sysloader, new Object[] {file.toURI().toURL()});
    System.out.println("CLASSPATH was extended by invoking addURL on sysloader.");
  }


  // =======================================================================
  

	// Don't need to save any state
	public boolean close() {
		return true;
	}

	// No special tests to determine if this plugin can run
	public boolean isSupported() {
		return true;
	}

  
	/**
	 * Initialize the plugin.
	 */
  public void init() {

    System.out.println("\n\n============ JRuby Adpater Initialization ============");

    String dflt=null;
    show_env_vars();
    init_properties();

    // We want to start up the Ruby ScriptEngine.
    // Doing so requires jar_path & ruby_std_lib.
    // These things can be set directly (see "Full control, low level" method), but
    // most of the time these paths are computed as a result of (for example) an rvm installation.
    // We want to define the fewest paths we can, and still compute jar_path & ruby_std_lib with adequate flexibility.
    // Once the ScriptEngine is started, the bulk of the configuration can be done in Ruby.

    rubyRoot = config_var("MD_RUBY_ROOT", home_dir() + "/.rvm/");

    runtime_type = config_var("MD_RUNTIME_TYPE", "rvm");
    
    rvm_jruby_version = config_var("MD_RVM_JRUBY_VERSION", find_jruby_version(), DEFAULT_RVM_JRUBY_VERSION);

    jar_jruby_version = config_var("MD_JAR_JRUBY_VERSION", rvm_jruby_version);
    
    jarchive = config_var("JARCHIVE", home_dir() + "/Jarchive/");
    
    dflt = ("jar".equals(runtime_type)) ? "" : rubyRoot + "/rubies/jruby-" + rvm_jruby_version + "/lib/"; 
    rubyLibPath = config_var("MD_RubyLibPath", dflt);
    
    // MagicDraw is supposed to put on the classpath all the jars specififed in the com.prometheus.rubyAdapter/plugin.xml file.
    // It appears, however, that the JRuby complete jar does not get put on the classpath. This lets us explicitly specify it.
    dflt = ("jar".equals(runtime_type)) ?  jarchive + "jruby-complete-" + jar_jruby_version + ".jar" : rubyLibPath + "jruby.jar";
    jar_path = config_var("MD_JAR_PATH", dflt);
    
    // Value for Java system property "org.jruby.embed.class.path”. Not used if empty string.
    // See https://github.com/jruby/jruby/wiki/RedBridge
    dflt = ("jar".equals(runtime_type)) ? "" : rubyLibPath + "ruby/stdlib";
    ruby_std_lib = config_var("MD_EMBED_CLASSPATH", dflt);
        
    try {
      
      // Add jruby jar to classpath. Needed only when jar is not in the plugin, such as when using rvm provided jruby.
      if (!"".equals(jar_path)) { addFileToClasspath(jar_path); }
      
      // Put rvm Ruby standard library on the load path.
      // http://stackoverflow.com/questions/14095744/embedding-jruby-in-java-code-using-rvm-jruby-install
      // NOTE: This does not seem to help at all, at least with loading things like fileutils from the stdlib. What does seem to work is to just add ruby_std_lib to $LOAD_PATH in the Ruby code.  Art wants it to still happen so we'll still try...of course that means we can't actually know if it is doing anything or not.  If we took it out and something broke then we would know.
      if (!"".equals(ruby_std_lib)) {
        System.setProperty("org.jruby.embed.class.path", ruby_std_lib);
        System.out.println("Set org.jruby.embed.class.path to " + ruby_std_lib );
      } else {
        System.out.println("DID NOT SET org.jruby.embed.class.path !!!" );
      }
	
	    // System.getenv("CLASSPATH") returns null, and also does not reflect dynamic additions
      // String class_path = System.getenv("CLASSPATH");
      String class_path = System.getProperty("java.class.path");

      System.out.println("rubyRoot:            " + rubyRoot);
      System.out.println("runtime_type:        " + runtime_type);
      System.out.println("rvm_jruby_version:   " + rvm_jruby_version);
	    System.out.println("jar_jruby_version:   " + jar_jruby_version);
      System.out.println("jarchive:            " + jarchive);
      System.out.println("jar_path:            " + jar_path);
      System.out.println("ruby_std_lib:        " + ruby_std_lib);

      System.out.println("");
      System.out.println("CLASSPATH:");
            for(String path : class_path.split(":"))
            {
                System.out.println("\t" + path + ":");
            }
	    System.out.println("");
	    System.out.println("============ Getting JRuby ScriptEngine =========");
	    System.out.println("");
      final ScriptEngine rubyEngine = new ScriptEngineManager().getEngineByName("jruby");
      if (null==rubyEngine) { throw new ScriptException("Error, could not find jruby ScriptEngine"); }
      
      // Ruby code to set up $LOAD_PATH and load plugins. Horribly ugly because Java's string handling is pathetic.
      final String ruby_code =
          // Define variables used by Java
          "runtime_type      = '" + runtime_type      + "'\n" +
          "rubyRoot          = '" + rubyRoot          + "'\n" +
          "rvm_jruby_version = '" + rvm_jruby_version + "'\n" +
          "jar_jruby_version = '" + jar_jruby_version + "'\n" +
          "rubyLibPath       = '" + rubyLibPath       + "'\n" +
          "ruby_std_lib      = '" + ruby_std_lib   + "'\n" +
          "gemset_name       = ENV['MD_GEMSET_NAME'] || 'MagicDraw' \n" +
          "dev_dir           = ENV['MD_DEV_DIR'] || '' \n" +
          // Compute variables that influence $LOAD_PATH
          "gemLibGlob        = \"" + rubyRoot + "gems/jruby-" + rvm_jruby_version + "@" + "#{gemset_name}" + "/gems/*/lib\"" + "\n" +
          "gemGlobalLibGlob  = \"" + rubyRoot + "gems/jruby-" + rvm_jruby_version + "@global/gems/*/lib\"" + "\n" +
          "gem_globs         = ENV['MD_GEM_GLOBS'] || [gemLibGlob, gemGlobalLibGlob].join(File::PATH_SEPARATOR)\n" +
          "extra_loadPaths   = ENV['MD_EXTRA_LOADPATHS']\n" +
          // Print variables that influence $LOAD_PATH
          "puts \"Variables that influence Ruby $LOAD_PATH:\"\n" +
          "puts \"\trubyRoot is #{rubyRoot.inspect} (can be set by MD_RUBY_ROOT. Used to compute defaults.)\"\n" +
          "puts \"\trvm_jruby_version is #{rvm_jruby_version.inspect} (can be set by MD_RVM_JRUBY_VERSION. Used to compute defaults.)\"\n" +
          "puts \"\tjar_jruby_version is #{jar_jruby_version.inspect} (can be set by MD_JAR_JRUBY_VERSION. Used to compute defaults.)\"\n" +
          "puts \"\trubyLibPath is #{rubyLibPath.inspect} (can be set by MD_rubyLibPath. Used to compute defaults.)\"\n" +
          "puts \"\truby_std_lib is #{ruby_std_lib.inspect} (can be set by MD_EMBED_CLASSPATH. Defines org.jruby.embed.class.path. See http://stackoverflow.com/questions/14095744/embedding-jruby-in-java-code-using-rvm-jruby-install)\"\n" +
          "puts \"\tgemset_name is #{gemset_name.inspect} (can be set by MD_GEMSET_NAME. Used to compute defaults.)\"\n" +
          "puts \"\tgem_globs is #{gem_globs.inspect} (can be set by MD_GEM_GLOBS. Used to find gem lib directories that are appended to $LOAD_PATH.)\"\n" +
          "puts \"\tdev_dir is #{dev_dir.inspect} (can be set by MD_DEV_DIR. Used to find lib directories that are PREPENDED to $LOAD_PATH.)\"\n" +
          "puts \"\textra_loadPaths is #{extra_loadPaths.inspect} (can be set by MD_EXTRA_LOADPATHS - appended to $LOAD_PATH.)\"\n" +
          // Setup $LOAD_PATH
          "unless dev_dir.empty?\n" +
          "\tdev_glob  = File.join(dev_dir, '*', 'lib')\n" +
          "\tdev_libs  = Dir[dev_glob]\n" +
          "\tdev_paths = dev_libs.join(File::PATH_SEPARATOR)\n" +
          "\tputs \"\tdev_paths is #{dev_paths.inspect} (computed from MD_DEV_DIR - prepended to $LOAD_PATH.)\"\n" +
          "\t$LOAD_PATH = dev_paths << $LOAD_PATH\n" +
          "end\n" +
          "gem_globs.split(File::PATH_SEPARATOR).each {|glob| Dir[glob].each {|path| $LOAD_PATH << path } }\n" +
          "extra_loadPaths.split(File::PATH_SEPARATOR).each {|path| $LOAD_PATH << path } if extra_loadPaths\n" +
          "$LOAD_PATH << ruby_std_lib\n" + // not sure why this is necessary.  Should have been taken care of by System.setProperty("org.jruby.embed.class.path", ruby_std_lib);
          // Print the $LOAD_PATH, for diagnoses
          "puts 'Ruby $LOAD_PATH: '\n" +
          "$LOAD_PATH.each { |path| puts \"\t#{path}\" }\n" +
          // Simplify the MagicDraw API for scripts. Includes code that loads plugins.
          "require 'magicdraw_extensions/magic_draw_glue'\n" +
          // Load plugins
          "include RubyAdapter; load_plugins\n";
          
      // System.out.println("===== ruby_code =====");
      // System.out.println(ruby_code);
      // System.out.println("==========");
      
      rubyEngine.eval(ruby_code);
    } catch (Throwable e) {
      System.out.println("");
      e.printStackTrace();
 	    System.out.println("");
    }
  }

	


}

# RubyAdaptor plugin for MagicDraw: lets you implement MagicDraw plugins in Ruby.
# Copyright (C) 2007  Prometheus Computing LLC

# This script
#   * Compiles the adapter
#   * Creates the jarfile
#   * Generates the plugin.xml that provides meta-information to MagicDraw
#   * Moves everyting into the necessary com.prometheus.rubyAdapter

# To use this script, you need to:
# * Point environmental variable 'MAGICDRAW_HOME' to your Magic Draw 18 installation directory
# * Point environmental varialbe 'JARCHIVE' to a directory that contains jruby-complete-9.2.7.0.jar
# * cd into the project directory
# * Run "rake compile"
# * Move the com.prometheus.rubyAdapter directory into your MagicDraw plugins directory

require 'fileutils'
include FileUtils::Verbose

def set_classpath
  ps = File::PATH_SEPARATOR
  md_dir = ENV['MAGICDRAW_HOME']
  #md_jar_names = %w{ md  md_common jide_action uml2 javax_jmi-1_0-fr cmof14  y } 
  md_jar_names = %w{
    CaliberRMSDK65.jar
    HTMLEditorLight.jar
    activation.jar
    antlr_2.7.6.1.jar
    batik.jar
    cmof14.jar
    commons-codec-1.3.jar
    commons-collections-3.1.jar
    commons-httpclient-3.1.jar
    commons-lang-2.1.jar
    commons-logging-1.0.4.jar
    cvsclient.jar
    fl_common.jar
    javax_jmi-1_0-fr.jar
    jax-qname.jar
    jaxb-api.jar
    jaxb-impl.jar
    jaxb-libs.jar
    jhall.jar
    jide-action.jar
    jide-common.jar
    jide-components.jar
    jide-dock.jar
    jide-editor.jar
    jide-grids.jar
    jide-shortcut.jar
    jimi.jar
    jmyspell-core-1.0.0-beta-2.jar
    launcher.jar
    log4j-1.2.15.jar
    md.jar
    md_api.jar
    md_common.jar
    md_common_api.jar
    namespace.jar
    oro-2.0.8.jar
    patch.jar
    relaxngDatatype.jar
    tw_common.jar
    tw_console.jar
    tw_console_api.jar
    uml2.jar
    velocity-1.6.2-dep.jar
    velocity-dep-1.5.jar
    xalan.jar
    xercesImpl.jar
    xfc.jar
    xml-apis.jar
    xsdlib.jar
    y.jar
  }
  md_path_parts = md_jar_names.collect {|jar_name| "#{md_dir}/lib/#{jar_name}" }
  md_path = md_path_parts.join(ps)
  ENV['CLASSPATH']= md_path <<  ps  <<  File.join(ENV['JARCHIVE'], 'jruby-complete-9.2.7.0.jar')
end

task :compile do
  set_classpath
  require './lib/rubyAdapter/meta_info'
  jar_name = "RubyAdapter_#{RubyAdapter::VERSION}.jar"
  system "javac -Xlint:deprecation -d ./  ./lib/com/prometheus/rubyAdapter/*.java"
  # The com/prometheus/rubyAdapter/ folder (containing .class files) is created during compilation.
  # Don't mistake this for:
  #    * lib/com/prometheus/rubyAdapter, which contains source
  #    * com.prometheus.rubyAdapter, which contains the finished ready-to-deploy plugin.
  system "jar -cf #{jar_name}  com/prometheus/rubyAdapter/*.class"
  # We don't want the class folder now that we have the jar
  rm_rf './com'
  output_dir = 'com.prometheus.rubyAdapter'
  rm_rf output_dir if Dir.exist?(output_dir)
  mkdir output_dir
  mv jar_name, output_dir
  template = File.read('lib/rubyAdapter/plugin.xml')
  plugin_metainfo = template.gsub('<PLUGIN-VERSION>', RubyAdapter::VERSION)
  File.open("#{output_dir}/plugin.xml", 'w') {|f| f << plugin_metainfo }
end


=begin
  # Would be nice to zip up the output, but don't want to add dependency on yet another gem.
  # Plus it's easy to zip up manually.
  require 'zip'
  Zip::File.open("#{output_dir}.zip", Zip::File::CREATE) { |zipfile|
     hierarchy = Dir[File.join(output_dir, '**', '**')]
    #  folders = hierarchy.select {|path| File.directory?(path) }
    #  folders = folders.sort
    #  folders.each { |path|
    hierarchy.each {|path|
        zipfile.add(path.sub(output_dir, ''), file)
        }
   }
=end